
// 2. Объявить массив с n-м количеством случайных чисел (явно инициализировать массив) и выбрать из него простые числа


public class Task2 {
    public static void main(String[] args) {
        int[] arr = {3, 7, 14, 13, 62, 100, 101};

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                System.out.println("Prime number: " + arr[i]);
            }
        }
    }
}
