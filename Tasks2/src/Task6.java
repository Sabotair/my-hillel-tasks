

/*6. Задать массив с n чисел. Найти числа, состоящее только из различных цифр*/

public class Task6 {

    public static boolean isDigitsDifferent(String number) {
        if (number.length() < 2) {
            return false;
        }
        for (int i = 0; i < number.length() - 1; i++) {
            for (int j = 1; j < number.length(); j++) {
                if (number.charAt(i) == number.charAt(j)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void printNumberWithDiffDigits(String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (isDigitsDifferent(array[i])) {
                System.out.println(array[i]);
            }
        }
    }

    public static void main(String[] args) {
        String[] numbers = {"5", "10", "222", "89", "75"};
        System.out.print("");
        printNumberWithDiffDigits(numbers);

    }
}
