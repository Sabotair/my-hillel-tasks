
/*5. Задать массив с n чисел. Найти самое короткое и самое длинное число. Вывести найденные числа и их длину.*/


public class Task5 {
    public static void main(String[] args) {

        int[] arr = {2, 5, 10, 12, 102, 564};

        for (int i = 0; i < arr.length; i++) {
            if ((arr[i] % 10) == arr[i]) {
                System.out.println("Length: " + getCountsOfDigits(arr[i]) + " | Shortest numbers: " + arr[i]);
            } else if ((arr[i] % 100) == arr[i]) {
            } else {
                System.out.println("Length: " + getCountsOfDigits(arr[i]) + " | Long numbers: " + arr[i]);
            }


        }
    }

    public static int getCountsOfDigits(long number) {
        int count = (number == 0) ? 1 : 0;
        while (number != 0) {
            count++;
            number /= 10;
        }
        return count;
    }
}
