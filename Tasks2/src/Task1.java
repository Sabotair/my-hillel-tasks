
// 1. Объявить массив с n-м количеством случайных чисел (явно инициализировать массив) и выбрать из него четные и нечетные числа.


public class Task1 {
    public static void main(String[] args) {
        int[] arr = {12, 4, 3, 33, 75, 85, 88};

        for (int i = 0; i < arr.length; i++) {

            if (arr[i] % 2 == 0) {
                System.out.println("Even numbers: " + arr[i]);
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 3 == 0) {
                System.out.println("Odd numbers: " + arr[i]);
            }
        }

    }
}
