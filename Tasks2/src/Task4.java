
// 4. Умножить две матрицы

public class Task4 {
    public static void main(String[] args) {
        int[][] arrOne = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        int[][] arrSecond = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] arrResultat = new int[arrOne.length][arrSecond.length];

        for (int i = 0; i < arrOne.length; i++) {
            for (int j = 0; j < arrSecond[0].length; j++) {
                arrResultat[i][j] = 0;
                for (int k = 0; k < arrOne[0].length; k++) {
                    arrResultat[i][j] += arrOne[i][k] * arrSecond[k][j];
                }
            }
        }

        for (int i = 0; i < arrResultat.length; i++) {
            for (int j = 0; j < arrResultat[i].length; j++) {
                System.out.print(" " + arrResultat[i][j] + " | ");
            }
            System.out.println();
        }
    }
}
