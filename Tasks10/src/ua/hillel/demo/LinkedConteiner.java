package ua.hillel.demo;


import java.util.StringJoiner;

public class LinkedConteiner<E> {

    private Node<E> fNode;
    private Node<E> lNode;
    private int size;

    public LinkedConteiner() {
        lNode = new Node<E>(null, fNode, null);
        fNode = new Node<E>(null, null, lNode);

    }

   /* public LinkedConteiner(E[] element) { // не работает с массивом
        for (E e : element) {
            addLast(e);
        }
    }*/

    public void addFirst(E e) {
        Node<E> next = fNode;
        next.setElement(e);
        fNode = new Node<E>(null, null, next);
        size++;
    }

    public void addLast(E e) {
        if (isEmpty()) {
            addFirst(e);
        } else {
            Node<E> prev = lNode;
            prev.setElement(e);
            lNode = new Node<E>(null, prev, null);
            prev.setNext(lNode);
            size++;
        }
    }

    public void add(int index, E element) {
        checkElementIndexForInsert(index);
        if (isFirstElementOperation(index)) {
            addFirst(element);
        } else if (isLastElementOperation(index)) {
            addLast(element);
        } else {
            Node<E> temp = getElementByIndex(index - 1);
            temp.next = new Node<E>(element, temp.getPrev(), temp.getNext());
            size++;
        }
    }

    public int size() {
        return size;
    }

    public Node<E> getElementByIndex(int index) {
        Node<E> target = fNode.getNext();
        for (int i = 0; i < index; i++) {
            target = target.getNext(target);

        }
        return target;
    }

    public E getElementByElement(int index) {
        Node<E> target = fNode.getNext();
        for (int i = 0; i < index; i++) {
            target = target.getNext(target);

        }
        return target.getElement();
    }

    public void removeFirst() {
        if (isNotEmpty()) {
            fNode = fNode.getNext();
            size--;
        }
    }

    public void removeLast() {
        if (size == 1) {
            removeFirst();
        } else if (isNotEmpty()) {
            Node<E> prev = (Node<E>) getElementByIndex(size - 2);
            prev.next = null;
            size--;
        }
    }

    public void remove(int index) {
        checkElementIndexForRemove(index);
        if (isFirstElementOperation(index)) {
            removeFirst();
        } else if (isLastElementOperation(index)) {
            removeLast();
        } else {
            Node<E> temp = getElementByIndex(index - 1);
            temp.next = temp.next.next;
            size--;
        }
    }

    public void replace(int firstIndex, int secondIndex) {
        checkElementIndexesForReplace(firstIndex, secondIndex);

        Node<E> firstIndexNode = getElementByIndex(firstIndex);
        Node<E> secondIndexNode = getElementByIndex(secondIndex);

        if (firstIndexNode == secondIndexNode) {
            return;
        }

        Node<E> firstIndexNodeBefore = getElementByIndex(firstIndex - 1);
        Node<E> secondIndexNodeBefore = getElementByIndex(secondIndex - 1);

        if (isFirstElementOperation(firstIndex)) {
            fNode = secondIndexNode;
        } else {
            firstIndexNodeBefore.next = secondIndexNode;
        }

        if (isFirstElementOperation(secondIndex)) {
            fNode = firstIndexNode;
        } else {
            secondIndexNodeBefore.next = firstIndexNode;
        }

        Node<E> temp = secondIndexNode.next;
        secondIndexNode.next = firstIndexNode.next;
        firstIndexNode.next = temp;

    }

    @Override
    public String toString() { // Последний элемент непонятный адрес объекта
        Node<E> temp = fNode;
        if (temp != null) {
            for (int i = 0; i < size; i++) {
                System.out.print("[" + getElementByElement(i) + "]" + ", ");
                temp = temp.getNext();
            }
        }
        return temp.toString();
    }

    private boolean isFirstElementOperation(int index) {
        return index == 0;
    }

    private boolean isLastElementOperation(int index) {
        return index == size;
    }


    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }

    private void checkElementIndexForRemove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(
                    "Index: " + index + ", Size: " + size);
        }
    }

    private void checkElementIndexForInsert(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException(
                    "Index: " + index + ", Size: " + size);
        }
    }

    private void checkElementIndexesForReplace(int firstIndex,
                                               int secondIndex) {
        checkElementIndexForRemove(firstIndex);
        checkElementIndexForRemove(secondIndex);
    }

    class Node<E> {
        private E element;
        private Node<E> next;
        private Node<E> prev;

        public Node(E element, Node prev, Node next) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }

        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }

        public Node<E> getPrev() {
            return prev;
        }

        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }

        public Node<E> getNext(Node<E> current) {
            return current.getNext();
        }
    }
}
