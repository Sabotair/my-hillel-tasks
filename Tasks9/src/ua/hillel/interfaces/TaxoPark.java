package ua.hillel.interfaces;

import ua.hillel.exeptions.CarExeption;

import java.util.ArrayList;

public interface TaxoPark {

    public double carPrice();

    public void sortByFuel();

    public ua.hillel.impl.Car[] speedRange(int min, int max) throws CarExeption;

    public ArrayList<ua.hillel.impl.Car> getCars();

    public void setCars(Car[] cars);
}
