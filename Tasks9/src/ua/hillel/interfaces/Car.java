package ua.hillel.interfaces;

public interface Car {
    String brand = null;
    String model = null;
    double fuel = 0;
    int speed = 0;
    int price = 0;

    String getBrand();

    String getModel();

    int getSpeed();

    double getPrice();

    double getFuel();

    void setBrand(String brand);

    void setModel(String model);

    void setFuel(double fuel);

    void setSpeed(int speed);

    void setPrice(int price);

    @Override
    String toString();

}
