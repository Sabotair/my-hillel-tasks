package ua.hillel.interfaces;

import java.util.ArrayList;

public interface CarsDataStorage {

    ArrayList<ua.hillel.impl.Car> read();

    void write(ArrayList<ua.hillel.impl.Car> cars);
}
