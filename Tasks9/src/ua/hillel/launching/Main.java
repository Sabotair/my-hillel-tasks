package ua.hillel.launching;

import ua.hillel.exeptions.CarExeption;
import ua.hillel.io.CarWrite;
import ua.hillel.io.Reader;
import ua.hillel.impl.Car;
import ua.hillel.impl.TaxoPark;

import java.util.ArrayList;


public class Main {

    public static void main(String[] args) {
        Reader reader = new Reader("Cars.txt", "CarWrite");
        ArrayList<Car> arrStr = reader.read();
        reader.write(arrStr);


        TaxoPark taxoPark = new TaxoPark(arrStr);

        System.out.println("Cost of all cars: " + taxoPark.carPrice() + "\n");
        taxoPark.sortByFuel();
        try {
            taxoPark.speedRange(-200, 250);
        } catch (CarExeption e) {
            System.out.println("Caught: " + e + "\n");
        }


        for (Car find : arrStr) {
            System.out.println(find);
        }
    }
}
