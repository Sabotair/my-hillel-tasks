package ua.hillel.io;

import ua.hillel.impl.Car;
import ua.hillel.impl.Hatchbacks;
import ua.hillel.impl.Pickups;
import ua.hillel.impl.Sedan;
import ua.hillel.interfaces.CarsDataStorage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringJoiner;


public class Reader implements CarsDataStorage {
    private String[] arrStr;
    private static final String CAR_LINE_DELIMETER = ";";
    private static final String CAR_PROPERTIES_DELEMETER = ",";

    private static final String SEDAN_CAR_CLASS_IDENTIFIER = "Sedan";
    private static final String PICKUPS_CLASS_IDENTIFIER = "Pickups";
    private static final String HATCHBACKS_CAR_CLASS_IDENTIFIER = "Hatchbacks";

    private String inputFileName;
    private String outputFileName;

    public Reader(String inputFileName, String outputFileName) {
        this.inputFileName = inputFileName;
        this.outputFileName = outputFileName;
    }

    @Override
    public ArrayList<Car> read() {
        ArrayList<Car> cars = null;

        try (FileInputStream stream = new FileInputStream(inputFileName)) {

            int symbol;
            String str;
            byte[] arr = new byte[10000];
            int i = 0;

            while ((symbol = stream.read()) != -1) {
                arr[i] = (byte) symbol;
                i++;
            }

            str = new String(arr).trim();
            String[] arrString = str.split(CAR_LINE_DELIMETER);
            arrStr = new String[arrString.length];
            for (int j = 0; j < arrStr.length; j++) {
                cars.set(j, createCarFromPropertiesArray(arrStr[j].split(CAR_PROPERTIES_DELEMETER)));
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return cars;
    }

    private Car createCarFromPropertiesArray(String[] carProperties) {
        Car[] car = null;

        switch (carProperties[0]) {
            case SEDAN_CAR_CLASS_IDENTIFIER:
                car[0] = new Sedan(carProperties);
                break;
            case PICKUPS_CLASS_IDENTIFIER:
                car[0] = new Pickups(carProperties);
                break;
            case HATCHBACKS_CAR_CLASS_IDENTIFIER:
                car[0] = new Hatchbacks(carProperties);
        }

        return car;
    }

    @Override
    public void write(ArrayList<Car> cars) {
        StringJoiner sj = new StringJoiner(CAR_LINE_DELIMETER);
        for (Car car : cars) {
            sj.add(car.toString());
        }
        try (FileOutputStream outputStream = new FileOutputStream(outputFileName)) {
            outputStream.write(sj.toString().getBytes());
        } catch (IOException e) {
            System.err.println("Error while writing file");
            e.printStackTrace();
        }
    }
    public String getInputFileName() {
        return inputFileName;
    }

    public void setInputFileName(String inputFileName) {
        this.inputFileName = inputFileName;
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public void setOutputFileName(String outputFileName) {
        this.outputFileName = outputFileName;
    }

}
