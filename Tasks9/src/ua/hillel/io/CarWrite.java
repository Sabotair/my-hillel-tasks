package ua.hillel.io;

import ua.hillel.impl.Car;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringJoiner;

public class CarWrite {
    private byte[] bytes;
    private static final String CAR_LINE_DELIMETER = ";";
    String fileName;

    public CarWrite(String fileName) {
        this.fileName = fileName;
    }

    /*public void write(String fileName) {

        try (FileOutputStream stream = new FileOutputStream(fileName)) {
            for (byte eachByte : bytes) {
                if (eachByte != '[' && eachByte != ']') {
                    stream.write(eachByte);
                }
            }
        } catch (IOException e) {
            System.out.println("Output error");
        }
    }*/
    public void write(ArrayList cars) {
        StringJoiner sj = new StringJoiner(CAR_LINE_DELIMETER);
        for (Object car : cars) {
            sj.add(car.toString());
        }
        try (FileOutputStream outputStream = new FileOutputStream(fileName)) {
            outputStream.write(sj.toString().getBytes());
        } catch (IOException e) {
            System.err.println("Error while writing file");
            e.printStackTrace();
        }
    }
}
