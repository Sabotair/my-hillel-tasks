package ua.hillel.impl;


public class Sedan extends Car {
    private int carryingCapacity;

    public Sedan(String brand, String model, int speed, int fuel, int price, int carryingCapacity) {
        super(brand, model, speed, fuel, price);
        this.carryingCapacity = carryingCapacity;
    }

    public Sedan(String[] arrStr) {
        super(arrStr[1], arrStr[2], Integer.parseInt(arrStr[3]), Integer.parseInt(arrStr[4]), Integer.parseInt(arrStr[5]));
    }

    @Override
    public String toString() {
        return super.toString() + carryingCapacity +
                ';';
    }
}
