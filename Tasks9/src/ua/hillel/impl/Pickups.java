package ua.hillel.impl;


public class Pickups extends Car {
    private int carryingCapacity;

    public Pickups(String brand, String model, int speed, int fuel, int price, int carryingCapacity) {
        super(brand, model, speed, fuel, price);
        this.carryingCapacity = carryingCapacity;
    }

    public Pickups(String[] carProperties) {
        super(carProperties[1], carProperties[2], Integer.parseInt(carProperties[3]), Integer.parseInt(carProperties[4]), Integer.parseInt(carProperties[5]));
    }

    @Override
    public String toString() {
        return super.toString() + carryingCapacity +
                ';';
    }


}
