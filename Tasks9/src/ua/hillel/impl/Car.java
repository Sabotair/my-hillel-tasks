package ua.hillel.impl;

public abstract class Car implements ua.hillel.interfaces.Car {
    private String brand;
    private String model;
    private double fuel;
    private int speed;
    private int price;

    public Car(String brand, String model, int speed, int fuel, int price) {
        this.brand = brand;
        this.model = model;
        this.speed = speed;
        this.price = price;
        this.fuel = fuel;

    }


    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getSpeed() {
        return speed;
    }

    public double getPrice() {
        return price;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setFuel(double fuel) {
        this.fuel = fuel;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getFuel() {
        return fuel;
    }

    @Override
    public String toString() {
        return        brand +
                "," + model +
                "," + speed +
                "," + price +
                "," + fuel +
                ',';
    }
}
