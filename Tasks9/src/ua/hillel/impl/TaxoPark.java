package ua.hillel.impl;

import ua.hillel.exeptions.CarExeption;

import java.util.ArrayList;

public class TaxoPark implements ua.hillel.interfaces.TaxoPark {
    private ArrayList<Car> cars;

    public TaxoPark(ArrayList<Car> car) {
        this.cars = car;
    }

    public double carPrice() {
        double sumPrice = 0;
        for (int i = 0; i < cars.size(); i++) {
            sumPrice += cars.get(i).getPrice();
        }
        return sumPrice;
    }

    public void sortByFuel() {
        Car x;
        for (int i = 0; i < cars.size(); i++) {
            for (int j = cars.size() - 1; j > i; j--) {
                if (cars.get(j - 1).getFuel() > cars.get(j).getFuel()) {
                    x = cars.get(j - 1);
                    cars.set(j - 1,  cars.get(j));
                    cars.set(j, x);
                }

            }
        }
    }

    public Car[] speedRange(int min, int max) throws CarExeption {
        Car[] tmp = new Car[1];
        Car[] res = new Car[1];
        if (min < 0 || max < 0) {
            throw new CarExeption();
        }

        for (int i = 0; i < cars.size(); i++) {
            if (min < cars.get(i).getSpeed() && cars.get(i).getSpeed() < max) {
                tmp[tmp.length - 1] = cars.get(i);
                res = new Car[tmp.length];
                for (int j = 0; j < res.length; j++) {
                    res[j] = tmp[j];
                }
                tmp = res;
            }
        }
        return res;
    }


    @Override
    public ArrayList<Car> getCars() {
        return cars;
    }

    @Override
    public void setCars(ua.hillel.interfaces.Car[] cars) {

    }


}
