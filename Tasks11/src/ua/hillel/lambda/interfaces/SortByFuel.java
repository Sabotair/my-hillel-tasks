package ua.hillel.lambda.interfaces;


import java.util.Comparator;

public interface SortByFuel<T> extends Comparator<T> {
    @Override
    int compare(T o1, T o2);
}
