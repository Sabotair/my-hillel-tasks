package ua.hillel.lambda.impl;


import ua.hillel.lambda.interfaces.SortByFuel;

import java.util.Arrays;
import java.util.Comparator;

public class TaxoPark {
    private Car[] cars;
    SortByFuel<Car> intrfeces = new SortByFuel<Car>() {
        @Override
        public int compare(Car o1, Car o2) {
            return (int) (o1.getFuel() - o2.getFuel());
        }
    };

    Comparator<Car> lambda = (o1, o2) -> (int) (o1.getFuel() - o2.getFuel());
    Comparator<Car> x = TaxoPark::sortByFuel;

    public TaxoPark(Car[] car) {
        this.cars = car;
    }


    public static int sortByFuel(Car car, Car car1) {
        return (int) (car.getFuel() - car.getFuel());
    }

    public double carPrice() {
        double sumPrice = 0;
        for (int i = 0; i < cars.length; i++) {
            sumPrice += cars[i].getPrice();
        }
        return sumPrice;
    }

    public void sortByFuel() {
        Arrays.sort(cars, x);
        Arrays.sort(cars, lambda);
        Arrays.sort(cars, intrfeces);
    }

    public Car[] speedRange(int min, int max) { // Err
        Car[] tmp = null;
        Car[] res = null;
        for (int i = 0; i < cars.length; i++) {
            if (min < cars[i].getSpeed() && cars[i].getSpeed() < max) {
                tmp[tmp.length - 1] = cars[i];
                res = new Car[tmp.length];
                for (int j = 0; j < res.length; j++) {
                    res[j] = tmp[j];
                }
                tmp = res;
                res = tmp;
            }
        }
        return res;
    }

    public Car[] getCars() {
        return cars;
    }

    public void setCars(Car[] cars) {
        this.cars = cars;
    }
}
