package ua.hillel.lambda.impl;

public class Pickups extends Car {
    private String type;

    public Pickups(int id, String brand, String model, int year, int speed, String color, double price, double fuel, String type) {
        super(id, brand, model, year, speed, color, price, fuel);
        this.type = type;
    }

    @Override
    public String toString() {
        return "Pickups{" + super.toString() +
                "type='" + type + '\'' +
                '}';
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
