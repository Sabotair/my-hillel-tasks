package ua.hillel.mylinked;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
         demoWithInitializedList();
        // demoWithEmptyList();
    }

    private static void demoWithInitializedList() {
        LinkedList<String> list = new LinkedList<>(
                new String[] { "1", "2", "3" });
        System.out
                .println("initial -> " + list + "; empty -> " + list.isEmpty());
        list.addLast("4");
        list.addLast("5");
        System.out.println("add two last -> " + list);
        list.addFirst("a");
        list.addFirst("b");
        System.out.println("add two first -> " + list);
        list.add(4, "q");
        list.add(6, "p");
        System.out.println(
                "add two by indexes: 'q' to 4 and 'p' to 6 -> " + list);
        list.removeFirst();
        list.removeLast();
        System.out.println("remove first and last -> " + list);
        list.remove(4);
        list.remove(2);
        System.out.println("remove by indexes 4 and 2 -> " + list);
        list.replace(0, 2);
        System.out.println("replace by indexes 0 and 2 -> " + list);
        list.replace(2, 4);
        System.out.println("replace by indexes 2 and 4 -> " + list);

        System.out.println(
                "size -> " + list.size() + "; empty -> " + list.isEmpty());
    }

    private static void demoWithEmptyList() {
        LinkedList<String> list = new LinkedList<>();
        System.out
                .println("initial -> " + list + " empty -> " + list.isEmpty());
        list.addLast("1");
        list.addLast("2");
        list.addLast("3");
        list.addLast("4");
        list.addLast("5");
        System.out.println("add five last -> " + list);
        list.addFirst("a");
        list.addFirst("b");
        System.out.println("add two first -> " + list);
        list.add(4, "q");
        list.add(6, "p");
        System.out.println(
                "add two by indexes: 'q' to 4 and 'p' to 6 -> " + list);
        list.removeFirst();
        list.removeLast();
        System.out.println("remove first and last -> " + list);
        list.remove(4);
        list.remove(2);
        System.out.println("remove by indexes 4 and 2 -> " + list);
        list.replace(0, 2);
        System.out.println("replace by indexes 0 and 2 -> " + list);
        list.replace(2, 4);
        System.out.println("replace by indexes 2 and 4 -> " + list);

        System.out.println(
                "size -> " + list.size() + "; empty -> " + list.isEmpty());
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
