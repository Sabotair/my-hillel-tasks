import java.io.*;
import java.util.*;

public class Reverses {
    public static void main(String[] args) {
        List list = new Reverses().toMap(new Reverses().listFromFile(new File("Text.txt")));
        System.out.println(list);
        Collections.reverse(list);
        System.out.println(list);
    }
    public List<String> listFromFile(File file) {

        List<String> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            String line;
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }


        return list;

    }

    public List toMap(List<String> list) {
        List<String> list1 = new ArrayList<>();


        list.forEach(line-> {
            String[] words = line.split(";");
            for (String word : words) {
                list1.add(word);
            }

        });
        return list1;
    }

}
