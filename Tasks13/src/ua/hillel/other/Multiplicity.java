import java.util.*;

public class Multiplicity {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        set1.add(4);
        set1.add(5);
        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(5);
        set2.add(6);
        set2.add(7);
        set2.add(8);
        System.out.println(new Multiplicity().union(set1, set2));
        System.out.println(new Multiplicity().intersect(set1, set2));

    }

    public Set union(Set set1, Set set2) {
        set1.addAll(set2);
        return set1;
    }

    public Set intersect(Set set1, Set set2) {
        set1.retainAll(set2);
        return set1;
    }
}
