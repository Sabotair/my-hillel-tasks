/*1. Создать класс по спецификации. Определить конструктор для всех полей. Создать отдельно класс процессор, который
будет выполнять определенные действия с объектамии и соджержать метод для печати объект. В методе main создать массив объектов и используя
класс процессор выполнить необходимые операции.

Car: id, Марка, Модель, Год выпуска, Цвет, Цена, Регистрационный номер.
Создать массив объектов. Вывести:
a) список автомобилей заданной марки;
b) список автомобилей заданной модели, которые эксплуатируются больше n лет;
c) список автомобилей заданного года выпуска, цена которых больше указанной.
*/

public class Main {
    public static void main(String[] args) {
        Car[] car = new Car[]{
                new Car(0, "Mazda", "RX-7", 2011, "Red", 22000, 44201),
                new Car(1, "Honda", "Accord", 2009, "Black", 12000, 8632),
                new Car(2, "Toyota", "RAW-4", 2005, "Green", 33000, 2221),
                new Car(3, "Mazda", "3", 2001, "Gray", 17000, 998)

        };

        Processor processor1 = new Processor(car);
        Processor processor = new Processor();


        processor1.brandOfMachine(car, "Mazda");
        processor1.modelExploitation(car, "Accord", 10);
        processor1.releaseYear(car, 2001, 10);

        System.out.println();

        processor1.brandOfMachine("Toyota");
        processor1.modelExploitation("Accord", 10);
        processor1.releaseYear(2001, 10);

        System.out.println();
        // Empty arr
        processor.brandOfMachine("Honda");
        processor.modelExploitation("Accord", 10);
        processor.releaseYear(2001, 10);

    }
}
