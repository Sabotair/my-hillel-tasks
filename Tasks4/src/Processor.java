/*1. Создать класс по спецификации. Определить конструктор для всех полей. Создать отдельно класс процессор, который
будет выполнять определенные действия с объектамии и соджержать метод для печати объект. В методе main создать массив объектов и используя
класс процессор выполнить необходимые операции.

Car: id, Марка, Модель, Год выпуска, Цвет, Цена, Регистрационный номер.
Создать массив объектов. Вывести:
a) список автомобилей заданной марки;
b) список автомобилей заданной модели, которые эксплуатируются больше n лет;
c) список автомобилей заданного года выпуска, цена которых больше указанной.
*/

public class Processor {
    Car[] cars;

    public Processor(Car[] cars) {
        this.cars = cars;
    }

    public Processor() {
        this.cars = new Car[10];
    }

    public void brandOfMachine(String brand) {

        try {
            System.out.println("Mark: " + brand);
            for (int i = 0; i < cars.length; i++) {
                if (brand.equals(cars[i].marka)) {
                    System.out.println("Id " + i + " | Marka: " + cars[i].marka + " | Model: " + cars[i].model + " | Year: " + cars[i].year + " | Color: " + cars[i].color + " | Price: " + cars[i].price + " | RegistrationNumber: " + cars[i].registrationNumber);
                }
            }
        } catch (NullPointerException n) {
            System.out.println("Empty arr");
        }
    }


    public void modelExploitation(String model, int age) {
        try {
            System.out.println("Model car: " + model + " | Age exploitation: " + age);
            for (int i = 0; i < cars.length; i++) {
                if (model.equals(cars[i].model) && age < cars[i].exploitation(2017)) {
                    System.out.println("Id " + i + " | Marka: " + cars[i].marka + " | Model: " + cars[i].model + " | Year: " + cars[i].year + " | Color: " + cars[i].color + " | Price: " + cars[i].price + " | RegistrationNumber: " + cars[i].registrationNumber);
                }
            }
        } catch (NullPointerException n) {
            System.out.println("Empty arr");
        }
    }

    public void releaseYear(int year, int price) {
        try {
            System.out.println("Release car: " + year + " | Price is more than specified: " + price);
            for (int i = 0; i < cars.length; i++) {
                if (year == cars[i].year && price <= cars[i].price) {
                    System.out.println("Id " + i + " | Marka: " + cars[i].marka + " | Model: " + cars[i].model + " | Year: " + cars[i].year + " | Color: " + cars[i].color + " | Price: " + cars[i].price + " | RegistrationNumber: " + cars[i].registrationNumber);
                }
            }
        } catch (NullPointerException n) {
            System.out.println("Empty arr");
        }

    }

    public void brandOfMachine(Car[] car, String brand) {
        System.out.println("Mark: " + brand);
        for (int i = 0; i < car.length; i++) {
            if (brand.equals(car[i].marka)) {
                System.out.println("Id " + i + " | Marka: " + car[i].marka + " | Model: " + car[i].model + " | Year: " + car[i].year + " | Color: " + car[i].color + " | Price: " + car[i].price + " | RegistrationNumber: " + car[i].registrationNumber);
            }
        }

    }

    public void modelExploitation(Car[] car, String model, int age) {
        System.out.println("Model car: " + model + " | Age exploitation: " + age);
        for (int i = 0; i < car.length; i++) {
            if (model.equals(car[i].model) && age < car[i].exploitation(2017)) {
                System.out.println("Id " + i + " | Marka: " + car[i].marka + " | Model: " + car[i].model + " | Year: " + car[i].year + " | Color: " + car[i].color + " | Price: " + car[i].price + " | RegistrationNumber: " + car[i].registrationNumber);
            }
        }
    }

    public void releaseYear(Car[] car, int year, int price) {
        System.out.println("Release car: " + year + " | Price is more than specified: " + price);
        for (int i = 0; i < car.length; i++) {
            if (year == car[i].year && price <= car[i].price) {
                System.out.println("Id " + i + " | Marka: " + car[i].marka + " | Model: " + car[i].model + " | Year: " + car[i].year + " | Color: " + car[i].color + " | Price: " + car[i].price + " | RegistrationNumber: " + car[i].registrationNumber);
            }
        }
    }

}

