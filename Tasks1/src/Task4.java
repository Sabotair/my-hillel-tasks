/*
        4. Ввести пароль из командной строки и сравнить его со строкой-образцом.
*/
public class Task4 {
    public static void main(String[] args) {
        String password = "password";

        if (args[0].equals(password))
            System.out.println("Password is correct!");
        else
            System.out.println("Password is incorrect!");
    }
}
