/*
        3. Объявить массив с n-м количеством случайных чисел (явно инициализировать массив)
        и вывести числа массива один раз с переходом(столбец) и один раз без перехода на новую строку(строка).
*/
public class Task3 {
    public static void main(String[] args) {

        int[] arr = new int[]{5,12,45,3,2,8,66,90,40};

        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        for (int i = 0; i < arr.length; i++) {

            System.out.print(arr[i] + " ");
        }
    }
}
