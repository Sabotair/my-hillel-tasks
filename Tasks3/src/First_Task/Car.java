package First_Task;

/*1. Создать класс по спецификации. Определить конструктор для всех полей. Создать отдельно класс процессор, который
будет выполнять определенные действия с объектамии и соджержать метод для печати объект. В методе main создать массив объектов и используя
класс процессор выполнить необходимые операции.

Car: id, Марка, Модель, Год выпуска, Цвет, Цена, Регистрационный номер.
Создать массив объектов. Вывести:
a) список автомобилей заданной марки;
b) список автомобилей заданной модели, которые эксплуатируются больше n лет;
c) список автомобилей заданного года выпуска, цена которых больше указанной.
*/

public class Car {

    int id;
    String marka;
    String model;
    int year;
    String color;
    double price;
    int registrationNumber;

    public Car(int id, String marka, String model, int year, String color, double price, int registrationNumber) {
        this.id = id;
        this.marka = marka;
        this.model = model;
        this.year = year;
        this.color = color;
        this.price = price;
        this.registrationNumber = registrationNumber;
    }

    public int exploitation(int year) {
        year += year - this.year;
        return year;
    }
}
