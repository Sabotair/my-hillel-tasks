package First_Task;
/*1. Создать класс по спецификации. Определить конструктор для всех полей. Создать отдельно класс процессор, который
будет выполнять определенные действия с объектамии и соджержать метод для печати объект. В методе main создать массив объектов и используя
класс процессор выполнить необходимые операции.

Car: id, Марка, Модель, Год выпуска, Цвет, Цена, Регистрационный номер.
Создать массив объектов. Вывести:
a) список автомобилей заданной марки;
b) список автомобилей заданной модели, которые эксплуатируются больше n лет;
c) список автомобилей заданного года выпуска, цена которых больше указанной.
*/

public class Main {
    public static void main(String[] args) {
        Processor processor = new Processor();
        processor.cars = new Car[4];
        processor.cars[0] = new Car(0, "Mazda", "RX-7", 2011, "Red", 22000, 44201);
        processor.cars[1] = new Car(1, "Honda", "Accord", 2009, "Black", 12000, 8632);
        processor.cars[2] = new Car(2, "Toyota", "RAW-4", 2005, "Green", 33000, 2221);
        processor.cars[3] = new Car(3, "Mazda", "3", 2001, "Gray", 17000, 998);

        processor.markCar("Mazda");
        System.out.println();
        processor.modelExploitation("Accord", 10);
        System.out.println();
        processor.releaseYear(2001, 10);


    }
}
