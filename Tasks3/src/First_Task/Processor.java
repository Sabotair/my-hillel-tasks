package First_Task;
/*1. Создать класс по спецификации. Определить конструктор для всех полей. Создать отдельно класс процессор, который
будет выполнять определенные действия с объектамии и соджержать метод для печати объект. В методе main создать массив объектов и используя
класс процессор выполнить необходимые операции.

Car: id, Марка, Модель, Год выпуска, Цвет, Цена, Регистрационный номер.
Создать массив объектов. Вывести:
a) список автомобилей заданной марки;
b) список автомобилей заданной модели, которые эксплуатируются больше n лет;
c) список автомобилей заданного года выпуска, цена которых больше указанной.
*/

public class Processor {
    Car[] cars;

    public void markCar(String name) {
        System.out.println("Mark: " + name);
        for (int i = 0; i < cars.length; i++) {
            if (name.equals(cars[i].marka)) {
                System.out.println("Id " + i + " | Marka: " + cars[i].marka + " | Model: " + cars[i].model + " | Year: " + cars[i].year + " | Color: " + cars[i].color + " | Price: " + cars[i].price + " | RegistrationNumber: " + cars[i].registrationNumber);
            }
        }
    }

    public void modelExploitation(String model, int age) {
        System.out.println("Model car: " + model + " | Age exploitation: " + age);
        for (int i = 0; i < cars.length; i++) {
            if (model.equals(cars[i].model) && age < cars[i].exploitation(2017)) {
                System.out.println("Id " + i + " | Marka: " + cars[i].marka + " | Model: " + cars[i].model + " | Year: " + cars[i].year + " | Color: " + cars[i].color + " | Price: " + cars[i].price + " | RegistrationNumber: " + cars[i].registrationNumber);
            }
        }
    }

    public void releaseYear(int year, int price) {
        System.out.println("Release car: " + year + " | Price is more than specified: " + price);
        for (int i = 0; i < cars.length; i++) {
            if (year == cars[i].year && price <= cars[i].price) {
                System.out.println("Id " + i + " | Marka: " + cars[i].marka + " | Model: " + cars[i].model + " | Year: " + cars[i].year + " | Color: " + cars[i].color + " | Price: " + cars[i].price + " | RegistrationNumber: " + cars[i].registrationNumber);
            }
        }
    }

}
