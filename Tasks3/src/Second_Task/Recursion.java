package Second_Task;
/*2. Дано натуральное число n (обычная переменная int).
    Рекурсивно вывести все числа от 1 до n. (Ввод 5 -> вывод 1 2 3 4 5, делать отдельным классом и в нем метод)*/


public class Recursion {

    void recursionNumber(int number) {
        if (number > 1) {
            recursionNumber(number - 1);
        }
        System.out.print(number + " ");
    }
}
