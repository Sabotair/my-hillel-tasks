package Second_Task;
/*3. Рекурсивно вычислить сумму цифр числа (делать отдельным классом и в нем метод)*/


public class RecursionSumNumber {
    int sum = 0;

    public int sumNumber(int n) {
        if (n == 0)
            return sum;
        else {
            sum += n % 10;
            return sumNumber(n /= 10);
        }
    }
}
