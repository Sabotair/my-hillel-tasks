package ua.hillel.Impl;

import ua.hillel.Exeptions.MyExeption;

public class TaxoPark implements ua.hillel.Interface.TaxoPark {
    private Car[] cars;

    public TaxoPark(Car[] car) {
        this.cars = car;
    }

    public double carPrice() {
        double sumPrice = 0;
        for (int i = 0; i < cars.length; i++) {
            sumPrice += cars[i].getPrice();
        }
        return sumPrice;
    }

    public void sortByFuel() {
        Car x;
        for (int i = 0; i < cars.length; i++) {
            for (int j = cars.length - 1; j > i; j--) {
                if (cars[j - 1].getFuel() > cars[j].getFuel()) {
                    x = cars[j - 1];
                    cars[j - 1] = cars[j];
                    cars[j] = x;
                }

            }
        }
    }

    public Car[] speedRange(int min, int max) throws MyExeption {
        Car[] tmp = new Car[1];
        Car[] res = new Car[1];
        if (min < 0 || max < 0) {
            throw new MyExeption();
        }

        for (int i = 0; i < cars.length; i++) {
            if (min < cars[i].getSpeed() && cars[i].getSpeed() < max) {
                tmp[tmp.length - 1] = cars[i];
                res = new Car[tmp.length];
                for (int j = 0; j < res.length; j++) {
                    res[j] = tmp[j];
                }
                tmp = res;
            }
        }
        return res;
    }


    @Override
    public Car[] getCars() {
        return cars;
    }

    @Override
    public void setCars(ua.hillel.Interface.Car[] cars) {
        this.cars = (Car[]) cars;
    }


}
