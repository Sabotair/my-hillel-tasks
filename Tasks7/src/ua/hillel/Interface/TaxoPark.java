package ua.hillel.Interface;

import ua.hillel.Exeptions.MyExeption;

public interface TaxoPark {

    public double carPrice();

    public void sortByFuel();

    public Car[] speedRange(int min, int max) throws MyExeption;

    public Car[] getCars();

    public void setCars(Car[] cars);
}
