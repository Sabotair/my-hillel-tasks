package ua.hillel.Interface;

public interface Car {
    int id = 0;
    String brand = null;
    String model = null;
    int year = 0;
    double fuel = 0;
    int speed = 0;
    String color = null;
    double price = 0;

    int getId();

    String getBrand();

    String getModel();

    int getYear();

    int getSpeed();

    String getColor();

    double getPrice();

    void setId(int id);

    void setBrand(String brand);

    void setModel(String model);

    void setYear(int year);

    void setFuel(double fuel);

    void setSpeed(int speed);

    void setColor(String color);

    void setPrice(double price);

    double getFuel();

    @Override
    String toString();

}
