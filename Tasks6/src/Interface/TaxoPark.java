package Interface;

public interface TaxoPark {

    public double carPrice();

    public void sortByFuel();

    public Car[] speedRange(int min, int max);

    public Car[] getCars();

    public void setCars(Car[] cars);
}
