import Interface.Car;

public class Main {

    public static void main(String[] args) {
        Car[] cars = {
                new Sedan(1, "Mazda", "RX-7", 2001, 250, "Red", 18000, 30, "Sedan"),
                new Sedan(2, "Toyota", "Corola", 2000, 150, "Red", 8000, 15, "Sedan"),
                new Sedan(3, "Tesla", "1", 2010, 211, "Red", 180000, 40, "Sedan"),
                new Hatchbacks(1, "Audi", "100", 2005, 233, "Red", 1000, 50, "Hatchbacks"),
                new Hatchbacks(2, "Audi", "TT", 2001, 155, "Red", 15000, 30, "Hatchbacks"),
                new Hatchbacks(3, "Mazda", "RX-7", 2001, 136, "Red", 11000, 40, "Hatchbacks"),
                new Pickups(1, "Hummer", "RX-7", 2001, 160, "Red", 134000, 20, "Pickups"),
                new Pickups(2, "Mazda", "R", 2001, 180, "Red", 17000, 10, "Pickups"),
                new Pickups(3, "Mazda", "M", 2001, 190, "Red", 19000, 25, "Pickups"),
        };

        TaxoPark taxoPark = new TaxoPark(cars);

        System.out.println("Cost of all cars: " + taxoPark.carPrice());
        taxoPark.sortByFuel();
        // taxoPark.speedRange(200, 250);  not correct


        for (Car find : cars) {
            System.out.println(find);
        }
    }
}
