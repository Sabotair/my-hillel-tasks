

public class Car implements Interface.Car {
    private int id;
    private String brand;
    private String model;
    private int year;
    private double fuel;
    private int speed;
    private String color;
    private double price;

    public Car(int id, String brand, String model, int year, int speed, String color, double price, double fuel) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.speed = speed;
        this.color = color;
        this.price = price;
        this.fuel = fuel;

    }

    public int getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }


    public int getYear() {
        return year;
    }

    public int getSpeed() {
        return speed;
    }

    public String getColor() {
        return color;
    }

    public double getPrice() {
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setFuel(double fuel) {
        this.fuel = fuel;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getFuel() {
        return fuel;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", fuel=" + fuel +
                ", speed=" + speed +
                ", color='" + color + '\'' +
                ", price=" + price +
                '}';
    }
}
